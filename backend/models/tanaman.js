const mongoose = require('mongoose');

var Tanaman = mongoose.model('Tanaman', {
    jenisTanaman: {
        type: String
    },
    jenisPertanian: {
        type: String
    },
    masaPanen: {
        type: String
    }
});

module.exports = {
    Tanaman
};