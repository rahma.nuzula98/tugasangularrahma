const mongoose = require('mongoose');

var Petani = mongoose.model('Petani', {
    nama: {
        type: String
    },
    jenisPetani: {
        type: String
    },
    alamat: {
        type: String
    },
    tanggalGabung: {
        type: String
    },
    tanggalLahir: {
        type: String
    },
    pendapatan: {
        type: Number
    }
});

module.exports = {
    Petani
};