const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var {
    Petani
} = require('../models/petani');

// => localhost:2121/employees/
router.get('/', (req, res) => {
    Petani.find((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error in Retriving Petani :' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Petani.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error in Retriving Petani :' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.post('/', (req, res) => {
    var emp = new Petani({
        nama: req.body.nama,
        jenisPetani: req.body.jenisPetani,
        alamat: req.body.alamat,
        tanggalGabung: req.body.tanggalGabung,
        tanggalLahir: req.body.tanggalLahir,
        pendapatan: req.body.pendapatan
    });
    emp.save((err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error in Petani Save :' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    var emp = {
        nama: req.body.nama,
        jenisPetani: req.body.jenisPetani,
        alamat: req.body.alamat,
        tanggalGabung: req.body.tanggalGabung,
        tanggalLahir: req.body.tanggalLahir,
        pendapatan: req.body.pendapatan
    };
    Petani.findByIdAndUpdate(req.params.id, {
        $set: emp
    }, {
        new: true
    }, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error in Petani Update :' + JSON.stringify(err, undefined, 2));
        }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Petani.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error in Petani Delete :' + JSON.stringify(err, undefined, 2));
        }
    });
});

module.exports = router;