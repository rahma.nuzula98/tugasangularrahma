const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const {
    mongose
} = require('./db.js');
var petaniController = require('./controllers/petaniController.js');
var tanamanController = require('./controllers/tanamanController.js');

var app = express();
app.use(bodyParser.json());
app.use(cors({
    origin: 'http://localhost:4200'
}));

app.listen(2222, () => console.log('Server started at port : 2222'));

app.use('/petanis', petaniController);
app.use('/tanamans', tanamanController);