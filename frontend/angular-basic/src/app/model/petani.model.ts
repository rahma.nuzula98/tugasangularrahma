export class Petani {
    _id: string;
    nama: string;
    jenisPertania: string;
    tanggalGabung: Date;
    tanggalLahir: Date;
    pendapatan: Date;
}