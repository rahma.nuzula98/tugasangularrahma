import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTanamanComponent } from './add-tanaman.component';

describe('AddTanamanComponent', () => {
  let component: AddTanamanComponent;
  let fixture: ComponentFixture<AddTanamanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTanamanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTanamanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
