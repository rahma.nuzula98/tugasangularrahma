import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TanamanService } from '../../../service/tanaman.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-tanaman',
  templateUrl: './add-tanaman.component.html',
  styleUrls: ['./add-tanaman.component.css']
})
export class AddTanamanComponent implements OnInit {

  tanamanfromdb: any;
  tanamanArr: any;
  constructor(private router: Router, private serviceService: TanamanService) { }

  ngOnInit() {
  }


  onSubmit() {
    this.tanamanArr = {
      "jenisTanaman": (document.getElementById("jenisTanaman") as HTMLInputElement).value,
      "jenisPertanian": (document.getElementById("jenisPertanian") as HTMLInputElement).value,
      "masaPanen": (document.getElementById("masaPanen") as HTMLInputElement).value
    }

    this.serviceService.postTanaman(this.tanamanArr).subscribe((res) => {
      alert("Add Success");
      this.router.navigate(['tanamans/']);
      console.log(res);
    });
  }

  back(): void {
    this.router.navigate(['tanamans/']);
  }
}
