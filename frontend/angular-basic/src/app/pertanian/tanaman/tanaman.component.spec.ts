import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TanamanComponent } from './tanaman.component';

describe('TanamanComponent', () => {
  let component: TanamanComponent;
  let fixture: ComponentFixture<TanamanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TanamanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TanamanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
