import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TanamanService } from '../../service/tanaman.service';
import { Tanaman } from '../../model/tanaman.model';

@Component({
  selector: 'app-tanaman',
  templateUrl: './tanaman.component.html',
  styleUrls: ['./tanaman.component.css']
})
export class TanamanComponent implements OnInit {

  tanamanfromdb: any;
  tanamanArr: any;
  constructor(private serviceTanaman: TanamanService, private router: Router) { }

  ngOnInit() {
    this.serviceTanaman.getTanaman().subscribe(data => {
      return this.tanamanfromdb = (data);
    });
  }

  // onSubmit() {
  //   this.tanamanArr = {
  //     "jenisTanaman": (document.getElementById("jenisTanaman") as HTMLInputElement).value,
  //     "jenisPertanian": (document.getElementById("jenisPertanian") as HTMLInputElement).value,
  //     "masaPanen": (document.getElementById("masaPanen") as HTMLInputElement).value
  //   }

  //     this.serviceTanaman.postTanaman(this.tanamanArr).subscribe((res) => {
  //   console.log(res);
  // });
  //   }

  editTanaman(tanaman): void {
    window.localStorage.removeItem("editTanamanId");
    window.localStorage.setItem("editTanamanId", tanaman._id + "");
    this.router.navigate(['tanamans/edit-tanaman/']);
  }

  addTanaman(tanaman): void {
    this.router.navigate(['tanamans/add-tanaman/']);
  }

  deleteTanaman(tanaman): void {
    console.log(tanaman);
    this.serviceTanaman.deleteTanaman(tanaman).subscribe((res) => {
      this.serviceTanaman.getTanaman().subscribe(data => {
        return this.tanamanfromdb = (data);
      });
    });
  }

}
