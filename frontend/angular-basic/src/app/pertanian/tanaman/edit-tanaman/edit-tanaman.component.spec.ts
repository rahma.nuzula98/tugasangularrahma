import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTanamanComponent } from './edit-tanaman.component';

describe('EditTanamanComponent', () => {
  let component: EditTanamanComponent;
  let fixture: ComponentFixture<EditTanamanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTanamanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTanamanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
