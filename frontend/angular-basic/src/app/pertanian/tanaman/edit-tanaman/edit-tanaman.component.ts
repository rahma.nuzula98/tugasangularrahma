import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TanamanService } from '../../../service/tanaman.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit-tanaman',
  templateUrl: './edit-tanaman.component.html',
  styleUrls: ['./edit-tanaman.component.css']
})
export class EditTanamanComponent implements OnInit {
  editForm: FormGroup;

  constructor(private router: Router, private serviceService: TanamanService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let tanamanId = window.localStorage.getItem("editTanamanId");
    if (!tanamanId) {
      this.router.navigate(['tanaman']);
      return;
    }
    this.editForm = this.formBuilder.group({
      _id: [],
      jenisTanaman: [],
      jenisPertanian: [],
      masaPanen: [],
      __v: []
    });

    this.serviceService.getTanamanById(tanamanId).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this.serviceService.putTanaman(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Success");
        this.router.navigate(['tanamans/']);
      }
      catch (err) {
        console.log(err.message);
      }
    });
  }

  back(): void {
    this.router.navigate(['tanamans/']);
  }


}
