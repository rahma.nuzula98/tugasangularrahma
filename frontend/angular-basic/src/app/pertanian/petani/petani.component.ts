import { Component, OnInit } from '@angular/core';
import { PetaniService } from '../../service/petani.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-petani',
  templateUrl: './petani.component.html',
  styleUrls: ['./petani.component.css']
})
export class PetaniComponent implements OnInit {

  petanifromdb: any;
  petaniArr: any;
  constructor(private servicePetani: PetaniService, private router: Router) { }

  ngOnInit() {
    this.servicePetani.getPetani().subscribe(data => {
      return this.petanifromdb = (data);
    });
  }

  // onSubmit() {
  //   this.petaniArr = {
  //     "nama": (document.getElementById("nama") as HTMLInputElement).value,
  //     "jenisPetani": (document.getElementById("jenisPetani") as HTMLInputElement).value,
  //     "alamat": (document.getElementById("alamat") as HTMLInputElement).value,
  //     "tanggalGabung": (document.getElementById("tanggalGabung") as HTMLInputElement).value,
  //     "tanggalLahir": (document.getElementById("tanggalLahir") as HTMLInputElement).value,
  //     "pendapatan": (document.getElementById("pendapatan") as HTMLInputElement).value
  //   }

  //   this.servicePetani.postPetani(this.petaniArr).subscribe((res) => {
  //     console.log(res);
  //   });
  // }

  editPetani(petani): void {
    window.localStorage.removeItem("editPetaniId");
    window.localStorage.setItem("editPetaniId", petani._id + "");
    this.router.navigate(['petanis/edit-petani/']);
  }

  addPetani(petani): void {
    this.router.navigate(['petanis/add-petani/']);
  }

  deletePetani(petani): void {
    console.log(petani);
    this.servicePetani.deletePetani(petani).subscribe((res) => {
      this.servicePetani.getPetani().subscribe(data => {
        return this.petanifromdb = (data);
      });
    });
  }
}
