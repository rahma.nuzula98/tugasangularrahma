import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PetaniService } from '../../../service/petani.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-petani',
  templateUrl: './add-petani.component.html',
  styleUrls: ['./add-petani.component.css']
})
export class AddPetaniComponent implements OnInit {

  petanifromdb: any;
  petaniArr: any;
  constructor(private router: Router, private serviceService: PetaniService) { }

  ngOnInit() {
    // this.serviceService.getPetani().subscribe(data => {
    //   return this.petanifromdb = (data);
    // });
  }

  onSubmit() {
    this.petaniArr = {
      "nama": (document.getElementById("nama") as HTMLInputElement).value,
      "jenisPetani": (document.getElementById("jenisPetani") as HTMLInputElement).value,
      "alamat": (document.getElementById("alamat") as HTMLInputElement).value,
      "tanggalGabung": (document.getElementById("tanggalGabung") as HTMLInputElement).value,
      "tanggalLahir": (document.getElementById("tanggalLahir") as HTMLInputElement).value,
      "pendapatan": (document.getElementById("pendapatan") as HTMLInputElement).value
    }

    this.serviceService.postPetani(this.petaniArr).subscribe((res) => {
      alert("Add Success");
      this.router.navigate(['petanis/']);
      console.log(res);
    });
  }

  back(): void {
    this.router.navigate(['petanis/']);
  }

}
