import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPetaniComponent } from './add-petani.component';

describe('AddPetaniComponent', () => {
  let component: AddPetaniComponent;
  let fixture: ComponentFixture<AddPetaniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPetaniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPetaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
