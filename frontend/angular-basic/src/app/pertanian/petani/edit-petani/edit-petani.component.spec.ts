import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPetaniComponent } from './edit-petani.component';

describe('EditPetaniComponent', () => {
  let component: EditPetaniComponent;
  let fixture: ComponentFixture<EditPetaniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPetaniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPetaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
