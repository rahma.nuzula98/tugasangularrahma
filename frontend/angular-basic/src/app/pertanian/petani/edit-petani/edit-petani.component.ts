import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PetaniService } from '../../../service/petani.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit-petani',
  templateUrl: './edit-petani.component.html',
  styleUrls: ['./edit-petani.component.css']
})
export class EditPetaniComponent implements OnInit {
  editForm: FormGroup;

  constructor(private router: Router, private serviceService: PetaniService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    let petaniId = window.localStorage.getItem("editPetaniId");
    if (!petaniId) {
      this.router.navigate(['petani']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id: [],
      nama: [],
      jenisPetani: [],
      alamat: [],
      tanggalGabung: [],
      tanggalLahir: [],
      pendapatan: [],
      __v: []
    });

    this.serviceService.getPetaniById(petaniId).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit() {
    this.serviceService.putPetani(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Success");
        this.router.navigate(['petanis/']);
      }
      catch (err) {
        console.log(err.message);
      }
    })
  }
  back(): void {
    this.router.navigate(['petanis/']);
  }

}
