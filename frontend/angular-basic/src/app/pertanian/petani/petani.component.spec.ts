import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaniComponent } from './petani.component';

describe('PetaniComponent', () => {
  let component: PetaniComponent;
  let fixture: ComponentFixture<PetaniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetaniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
