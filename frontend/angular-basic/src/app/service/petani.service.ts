import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServiceResponse } from '../model/service.response';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { from } from 'rxjs/observable/from';
import { PetaniComponent } from '../pertanian/petani/petani.component';
import { Petani } from '../model/petani.model';

@Injectable()
export class PetaniService {

  BASE_URL: string = 'http://localhost:2222/petanis/'

  constructor(private http: HttpClient) { }

  getPetani(): Observable<any> {
    return this.http.get<any>(this.BASE_URL);
  }

  postPetani(emp: Petani) {
    return this.http.post(this.BASE_URL, emp);
  }

  deletePetani(emp) {
    return this.http.delete(this.BASE_URL + emp._id);
  }

  getPetaniById(empId: String): Observable<any> {
    return this.http.get<any>(this.BASE_URL + empId);
  }

  putPetani(emp: Petani) {
    return this.http.put(this.BASE_URL + emp._id, emp);
  }

}
