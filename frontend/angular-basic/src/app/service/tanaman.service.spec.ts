import { TestBed, inject } from '@angular/core/testing';

import { TanamanService } from './tanaman.service';

describe('TanamanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TanamanService]
    });
  });

  it('should be created', inject([TanamanService], (service: TanamanService) => {
    expect(service).toBeTruthy();
  }));
});
