import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServiceResponse } from '../model/service.response';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { from } from 'rxjs/observable/from';
import { TanamanComponent } from '../pertanian/tanaman/tanaman.component';
import { Tanaman } from '../model/tanaman.model';

@Injectable()
export class TanamanService {

  BASE_URL: string = 'http://localhost:2222/tanamans/'

  constructor(private http: HttpClient) { }
  getTanaman(): Observable<any> {
    return this.http.get<any>(this.BASE_URL);
  }

  postTanaman(emp: Tanaman) {
    return this.http.post(this.BASE_URL, emp);
  }

  deleteTanaman(emp) {
    return this.http.delete(this.BASE_URL + emp._id);
  }

  getTanamanById(empId: String): Observable<any> {
    return this.http.get<any>(this.BASE_URL + empId);
  }

  putTanaman(emp: Tanaman) {
    return this.http.put(this.BASE_URL + emp._id, emp);
  }

}
