import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { PetaniComponent } from './pertanian/petani/petani.component';
import { FormGroup, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TanamanComponent } from './pertanian/tanaman/tanaman.component';
import { PetaniService } from '../../src/app/service/petani.service';
import { KomponenComponent } from './komponen/komponen.component';
import { HeaderComponent } from './komponen/header/header.component';
import { FooterComponent } from './komponen/footer/footer.component';
import { SidebarComponent } from './komponen/sidebar/sidebar.component';
import { DashbordComponent } from './komponen/dashbord/dashbord.component';
import { EditPetaniComponent } from './pertanian/petani/edit-petani/edit-petani.component';
import { AddPetaniComponent } from './pertanian/petani/add-petani/add-petani.component';
import { TanamanService } from './service/tanaman.service';
import { AddTanamanComponent } from './pertanian/tanaman/add-tanaman/add-tanaman.component';
import { EditTanamanComponent } from './pertanian/tanaman/edit-tanaman/edit-tanaman.component';

const appRoutes: Routes = [
  {
    path: "",
    redirectTo: "dashbord",
    pathMatch: "full"
  },
  {
    path: "dashbord",
    component: DashbordComponent
  },
  {
    path: "petanis",
    component: PetaniComponent
  },
  {
    path: "petanis/add-petani",
    component: AddPetaniComponent
  },
  {
    path: "petanis/edit-petani",
    component: EditPetaniComponent
  },
  {
    path: "tanamans",
    component: TanamanComponent
  }, {
    path: "tanamans/add-tanaman",
    component: AddTanamanComponent
  },
  {
    path: "tanamans/edit-tanaman",
    component: EditTanamanComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    PetaniComponent,
    TanamanComponent,
    KomponenComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    DashbordComponent,
    EditPetaniComponent,
    AddPetaniComponent,
    AddTanamanComponent,
    EditTanamanComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PetaniService, TanamanService],
  bootstrap: [AppComponent]
})
export class AppModule { }
